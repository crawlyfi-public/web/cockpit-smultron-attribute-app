<?php

namespace Cockpit\Controller;

class AttributeAppController extends \Cockpit\AuthController {
    public function index() {
        return $this->render('attribute-app:views/index.php', compact('current', 'groups'));
    }
}
