



<div>
    <ul class="uk-breadcrumb">
        <li><a href="@route('/settings')">@lang('PLM')</a></li>
        <li class="uk-active"><span>@lang('Attribute APP')</span></li>
    </ul>
</div>


<div class="iframe-container">
  <iframe id="IframeId" src="https://test.plm.attribute-app.smultron.app/?api=https://test.plm.attribute-api.smultron.app/api/mappingRule" style="width:100%;" onload="setIframeHeight(this)" ></iframe>
</div>