<?php

require_once(__DIR__ . '/Controller/AttributeAppController.php');

/**
 * register assets
 */
$app['app.assets.base'] = array_merge($app['app.assets.base'], [
    'attribute-app:assets/css/groups.css',
    'attribute-app:assets/js/groups.js',
]);


/**
 * register routes
 */
$app->bind('/attribute-app', function() {
    return $this->invoke('Cockpit\\Controller\\AttributeAppController', 'attribute-app');
 });
 $app->bindClass('Cockpit\\Controller\\AttributeAppController', 'attribute-app');

/**
 * on admint init
 */
$app->on('admin.init', function() {}, 0);


/*
 * add menu entry if the user has access to group stuff
 */
$this->on('cockpit.menu', function() {
    $this->renderView("attribute-app:views/partials/menu.php", ['module' => $this->module('groups')]);    
});

// ...
$app('admin')->init();
